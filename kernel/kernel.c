#include <vga.h>
#include <gdt.h>
#include <idt.h>

void kmain()
{
    kclear();
    gdt_install();
    klog("Entry", "GDT installed!");
    idt_install();
    klog("Entry", "IDT installed!");
    kdisplay_logo();
    for (;;);
}