#ifndef TYPES_H
#define TYPES_H

#define NULL (void*)0

typedef unsigned long int u64;
typedef signed long int s64;
typedef unsigned int u32;
typedef signed int s32;
typedef unsigned short u16;
typedef signed short s16;
typedef unsigned char u8; 
typedef signed char s8;

#endif