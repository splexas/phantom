#include <vga.h>

static u32 cursor_x = 0;
static u32 cursor_y = 0;

void kclear()
{
    struct vga_char* video = (struct vga_char*)VIDEO_MEMORY;
    for (int i = 0; i < VIDEO_WIDTH * VIDEO_HEIGHT; i++)
        video[i].character = ' ';
}

void kprint(s8 color, const s8* msg)
{
    struct vga_char* video = (struct vga_char*)VIDEO_MEMORY;
    u32 index = cursor_x + cursor_y * VIDEO_WIDTH;
    while (*msg != 0) {
        if (cursor_x == VIDEO_WIDTH || *msg == '\n') {
            cursor_x = 0;
            cursor_y += 1;
            index = cursor_x + cursor_y * VIDEO_WIDTH;
            if (*msg == '\n') {
                msg++;
                continue;
            }
        }
        video[index].character = *msg++;
        video[index].color = color;
        index++;
        cursor_x++;
    }
}

void klog(const s8* tag, const s8* msg)
{
    kprint(VGA_WHITE, tag);
    kprint(VGA_WHITE, ": ");
    kprint(VGA_GRAY, msg);
    kprint(VGA_GRAY, "\n");
}

void kdisplay_logo()
{
    const s8* logo = (
        " _____  _                 _\n"                  
        "|  __ \\| |               | |\n"                 
        "| |__) | |__   __ _ _ __ | |_ ___  _ __ ___\n"
        "|  ___/| '_ \\ / _` | '_ \\| __/ _ \\| '_ ` _ \\\n" 
        "| |    | | | | (_| | | | | || (_) | | | | | |\n"
        "|_|    |_| |_|\\__,_|_| |_|\\__\\___/|_| |_| |_|\n"
    );
    kprint(VGA_WHITE, logo);
}